
-- SUMMARY --

This module is basically a gateway between a Drupal commerce website and Billdesk.

The Billdesk Payment Gateway module implements the version of Billdesk payment processing service (https://www.billdesk.com) in Drupal Commerce.

As per i know Billdesk doesn't support test/dummy transactions. So please test this module in your local development setup with Rs.1 per transaction and then go head if everything is fine.

-- WARNING --

This module is purely for Testing purpose donot use this module directly in your LIVE Environment.

Test this in you local environment and also the parameters that Billdesk given to you

-- REQUIREMENTS --

dependencies[] = commerce
dependencies[] = commerce_ui
dependencies[] = commerce_payment
dependencies[] = commerce_order


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

* You likely want to disable Toolbar module, since its output clashes with
  Administration menu.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Use the administration pages and help (System module)

    The top-level administration categories require this permission to be
    accessible. The administration menu will be empty unless this permission is
    granted.


-- CONTACT --

Current maintainers:
* Ajay Kumar Reddy - https://www.drupal.org/u/ajaykumarreddy1392
* Viraj Rajankar - https://www.drupal.org/u/virajrajankar


